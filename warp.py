#!/usr/bin/python
# -*- coding: utf-8 -*-

VERSION = "v0.1 (poc code)"

"""
Copyright (c) 2013 devunt

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
"""
from gevent.monkey import patch_all; patch_all()

import io
import logging
import optparse
import random
import socket
import time

from gevent.pool import Pool
from gevent.pywsgi import WSGIServer


def application(environ, start_response):
    logger = logging.getLogger(__name__ + '.application')
    logger.debug('Accept new task')

    phost = False

    phost = environ.get('HTTP_HOST', '127.0.0.1')
    path = environ['PATH_INFO']
    path = path[path.find('/', 8):]
    protocol = environ['SERVER_PROTOCOL']
    if protocol.endswith('/1.0'):
        protocol = protocol[:-1] + '1'

    logger.debug('Process - %s %s %s',
                 environ['REQUEST_METHOD'],
                 environ['PATH_INFO'],
                 protocol)

    new_head = ' '.join([environ['REQUEST_METHOD'], path, protocol])

    try:
        host, port = phost.split(':', 1)
    except ValueError:
        host = phost
        port = 80
        phost = host + ':80'
    else:
        port = int(port)

    logger.debug('environ = %r', environ)

    headers = dict((k[5:].replace('_', '-'), v)
                   for k, v in environ.iteritems()
                   if k.startswith('HTTP_'))
    try:
        del headers['HOST']
    except KeyError:
        pass
    try:
        del headers['ACCEPT-ENCODING']
    except KeyError:
        pass
    for key in headers:
        if key.startswith('PROXY-'):
            del headers[key]

    try:
        req_sc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        req_sc.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        req_sc.connect((host, port))
        if logger.isEnabledFor(logging.DEBUG):
            def send(value):
                for line in value.split('\r\n'):
                    logger.debug(line)
                req_sc.send(value)
        else:
            send = req_sc.send
        send(new_head + '\r\n')

        time.sleep(0.2)

        send('Host: ')
        def feed_phost(phost):
            i = 1
            while phost:
                yield random.randrange(2, 4), phost[:i]
                phost = phost[i:]
                i = random.randrange(2, 5)
        for delay, c in feed_phost(phost):
            time.sleep(delay/10.0)
            send(c)
        send('\r\n')

        send('\r\n'.join(k.title() + ': ' + v for k, v in headers.iteritems()))
        if 'CONNECTION' not in headers:
            send('\r\nConnection: close')
        send('\r\n\r\n')
    except Exception as e:
        logger.exception(e)
        raise

    header = io.BytesIO()
    while True:
        try:
            buf = req_sc.recv(1024)
            logger.debug('> %r', buf)
            if not buf:
                break
            if header is None:
                yield buf
                continue
            header.write(buf)
            header_string = header.getvalue()
            try:
                content_offset = header_string.index('\r\n\r\n')
            except ValueError:
                pass
            else:
                start_response(*parse_header(header_string[:content_offset]))
                yield header_string[content_offset + 4:]
                header.close()
                header = None
        except Exception as e:
            logger.exception(e)
            raise

    req_sc.close()

    logger.debug('Task done')


def parse_header(header_string):
    logger = logging.getLogger(__name__ + '.parse_header')
    lines = header_string.split('\r\n')
    status = ' '.join(lines[0].split()[1:])
    headers = []
    for line in lines[1:]:
        if line.startswith((' ', '\t')):
            if headers:
                headers[-1] += line
            continue
        headers.append(tuple(line.split(':', 1)))
    logger.debug('status = %r, headers = %r', status, headers)
    return status, headers


def main():
    """CLI frontend function.  It takes command line options e.g. host,
    port and provides ``--help`` message.

    """
    parser = optparse.OptionParser(description='Simple HTTP transparent proxy',
                                   version=VERSION)
    parser.add_option('-H', '--host', default='127.0.0.1',
                      help='Host to listen [%default]')
    parser.add_option('-p', '--port', type='int', default=8800,
                      help='Port to listen [%default]')
    parser.add_option('-c', '--count', type='int', default=64,
                      help='The number of green threads to spawn [%default]')
    parser.add_option('-v', '--verbose', action="store_true",
                      help='Print verbose')
    options, args = parser.parse_args()
    if not (1 <= options.port <= 65535):
        parser.error('port must be 1-65535')
    if options.verbose:
        lv = logging.DEBUG
    else:
        lv = logging.INFO
    logging.basicConfig(level=lv, format='[%(asctime)s] {%(levelname)s} %(message)s')
    pool = Pool(options.count)
    server = WSGIServer((options.host, options.port), application, spawn=pool)
    try:
        return server.serve_forever()
    except KeyboardInterrupt:
        print 'bye'


if __name__ == '__main__':
    exit(main())
